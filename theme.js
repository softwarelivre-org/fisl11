(function($) {

  var $translations = {
    en: {
      'prefix': 'english',
      "Sobre o evento": ["About the event", "about-the-event"],
      "Entre na Rede Social": ["Join the social network"],
      "Blog de Notícias": ["News blog"],
      "Agenda": ["Agenda", "agenda"],
      "Agência de Turismo": ["Tourism Agency", 'tourism-agency'],
      "Inscrições": ["Registration"],
      "Inscrições On Line": ["Online registration", "registration"],
      "Submissão WSL": ["WSL submissions", 'wsl-submissions'],
      "Submissão Palestras": ["Talk submissions", 'talk-submissions'],
      "Assinaturas de Chave": ["Key signing", 'key-signing'],
      "Participe": ["Attend"],
      "Divulgação": ["Spread the word", 'spread-the-word'],
      "Caravanas": ["Caravans", 'caravans'],
      "Mobilização": ["Mobilization", 'mobilization'],
      "Convide amig@s!": ["Invite your friends"],
      "Páginas Amigas": ["Friend pages", 'friend-pages'],
      "Grupo de Usuários": ["User groups", 'user-groups'],
      "Seja Patrocinador": ["Be a sponsor", 'be-a-sponsor'],
      "Serviços": ["Services"],
      "Comitê Organizador": ["Organizing Committee", 'organizing-comittee'],
      "Assessorias": ["Service providers", 'service-providers'],
      "Planta do fisl11": ["Event map", 'event-map'],
      "Trasmissão": ['Broadcasting'],
      'FISL na WEB': ['FISL on the WEB'],
      'Fãs do FISL!': ['FISL fans!'],
      'Novidades!': ['News!'],
      'Notícias' : ['News', 'news'],
      'Apresentação' : ['Presentation', 'presentation'],
      'Sala de Imprensa' : ['Press Room'],
      'Cadastro de Imprensa' : ['Press Registration', 'press-registration'],
      'Envie sua notícia' : ['Send your news', 'send-your-news'],
    },
    es: {
      'prefix': 'espanol',
      "Sobre o evento": ["Sobre el evento", "sobre-el-evento"],
      "Entre na Rede Social": ["Únete a la Red Social"],
      "Blog de Notícias": ["Blog de Noticias"],
      "Agenda": null,
      "Agência de Turismo": ["Agencia de Viajes", "agencia-de-viajes"],
      "Inscrições": ["Suscripciones"],
      "Inscrições On Line": ["Inscripción On Line", "inscripcion-on-line"],
      "Submissão WSL": ["Presentación de WSL", "presentacion-de-wsl"],
      "Submissão Palestras": ["Presentación de Ponencias", "presentacion-de-ponencias"],
      "Assinaturas de Chave": ["Firmado de claves", "firmado-de-claves"],
      "Participe": ["Participar"],
      "Divulgação": ["Divulgación", "divulgacion"],
      "Caravanas": ["Caravanas", "caravanas"],
      "Mobilização": ["Movilización", "movilizacion"],
      "Convide amig@s!": ["Invita tus amigos"],
      "Páginas Amigas": ["Sítios amigos", "sitios-amigos"],
      "Grupo de Usuários": ["Grupo de Usuarios", "grupo-de-usuarios"],
      "Seja Patrocinador": ["Sea un patrocinador", "sea-un-patrocinador"],
      "Serviços": ['Servicios'],
      "Comitê Organizador": ["Comité Organizador", "comite-organizador"],
      "Assessorias": ["Asesoramiento", "asesoramiento"],
      "Planta do fisl11": ["Planta fisl11", "planta-fisl11"],
      "Trasmissão": ["Transmisión"],
      'FISL na WEB': ["FISL en la web"],
      'Fãs do FISL!': ["Los fans de la FISL!"],
      'Novidades!': null,
      'Notícias' : ['Noticias', 'noticias'],
      'Apresentação' : ['Presentación', 'presentacion'],
      'Sala de Imprensa' : ['Salón de Prensa'],
      'Cadastro de Imprensa' : ['Registro de Prensa', 'registro-de-prensa'],
      'Envie sua notícia' : ['Envíe sus noticias', 'envie-sus-noticias'],
    },
  }

  var language = $('html').attr('lang');
  if ($translations[language]) {
    var prefix = $translations[language]['prefix'];
    $('.link-list-block a, .block h3').each(function() {
      var element = $(this);
      var translated = $translations[language][element.html()];
      if (translated) {
        element.html(translated[0]);
        if (element.attr('href') && translated[1]) {
          element.attr('href', '/fisl11/' + prefix + "/" + translated[1]);
        }
      }
    });
  }

  url = document.location.href
  if (url.match(/^http:\/\/softwarelivre.org\/fisl11\/?(\?lang.*)?$/)) {
    if (language=='en') {
      document.location = 'http://softwarelivre.org/fisl11/english/news';
    } else if (language=='es') {
      document.location = 'http://softwarelivre.org/fisl11/espanol/noticias';
    }
  }

})(jQuery);
